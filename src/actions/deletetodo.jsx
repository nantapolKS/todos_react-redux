export const DEL_TODO = 'DEL_TODO'

export function del_todo(id) {
    return { type: DEL_TODO, id: id }
}
