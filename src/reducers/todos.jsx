import { ADD_TODO } from '../actions/addtodo'
import { DEL_TODO } from '../actions/deletetodo'

let todoslist = [
    {
        id: 0,
        text: 'Use Redux'
    }
]

function todos(state = todoslist, action) {
    switch (action.type) {
        case ADD_TODO:
            return [...state, {
                id: state.reduce((maxId, todo) => Math.max(todo.id, maxId), -1) + 1,
                text: action.text
            }]
        case DEL_TODO:
            return state.filter(todo => todo.id !== action.id)
        default:
            return state
    }
}

export default todos
