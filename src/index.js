import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware } from "redux"
import { Provider } from 'react-redux'
import rootReducer from './reducers'
import logger from 'redux-logger'

import App from './components/App'

import './index.css'

const createStoreWithMiddleware = applyMiddleware(logger)(createStore)

ReactDOM.render(
    <Provider store={createStoreWithMiddleware(rootReducer)}>
        <App />
    </Provider>, document.getElementById('root')
)