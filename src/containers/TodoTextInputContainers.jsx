import { connect } from 'react-redux'
import { add_todo } from '../actions/addtodo'
import TodoTextInput from '../components/TodoTextInput'

const mapDispatchToProps = {
    add_todo
}

export default connect(null, mapDispatchToProps)(TodoTextInput)
