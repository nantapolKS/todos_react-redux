import { connect } from 'react-redux'
import TodoList from '../components/TodoList'
import { del_todo } from '../actions/deletetodo'

const mapStateToProps = state => ({
    todos: state.todos
})

const mapDispatchToProps = {
    del_todo
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)