import React from 'react'

const TodoItem = (props) => (
    <div className="todo__item">
        <h3>{props.todo.text}</h3>
        <button className="destroy" onClick={() => props.del_todo(props.todo.id)}>X</button>
    </div>
)

export default TodoItem
