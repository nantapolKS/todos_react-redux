import React from 'react'

import Header from './Header'
import TodoTextInputContainers from '../containers/TodoTextInputContainers'
import TodoListContainers from '../containers/TodoListContainers'

const App = () => (
  <div className="warppar">
    <Header />
    <div className="todo">
      <TodoTextInputContainers />
      <TodoListContainers />
    </div>
  </div>
)

export default App
