import React from 'react'

const Header = () => (
    <div className="header">
        <h1>TODOS</h1>
    </div>
)

export default Header