import React from 'react'
import TodoItem from './TodoItem'

const TodoList = (props) => (
    <div className="todo_list">
        {props.todos.map(todo => <TodoItem key={todo.id} todo={todo} del_todo={props.del_todo} />)}
    </div>
)

export default TodoList
