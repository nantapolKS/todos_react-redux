import React from 'react'

class TodoTextInput extends React.Component {

    constructor(props) {
        super(props)

        this.state = { text: '' }
    }

    handleSubmit = e => {
        const text = e.target.value.trim()
        if (!text) return
        if (e.which === 13) {
            this.props.add_todo(text)
            this.setState({ text: '' })
        }
    }

    handleChange = e => {
        this.setState({ text: e.target.value })
    }

    render() {
        return (
            <input
                className="new-todo"
                type="text"
                placeholder="What needs to be done?"
                value={this.state.text}
                onChange={this.handleChange}
                onKeyDown={this.handleSubmit}
            />
        )
    }
}

export default TodoTextInput